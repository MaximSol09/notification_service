from rest_framework import serializers
from .models import Client, Mailing, Message


class ClientSetSerializer(serializers.ModelSerializer):
    """ Сериализатор модели клиент """
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Client
        fields = ('id',  'phone', 'mobile_code', 'tag', 'time_zone')


class UpdateClientSerializer(serializers.ModelSerializer):
    """ Сериализатор модели клиент """
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Client
        fields = ('id',  'phone', 'mobile_code', 'tag', 'time_zone')


class MailingSerializer(serializers.ModelSerializer):
    """ Сериализатор модели рассылки """
    id = serializers.IntegerField(read_only=True)
    filter_mobile_code = serializers.CharField(max_length=50, required=False)
    filter_tag = serializers.CharField(max_length=50, required=False)

    class Meta:
        model = Mailing
        fields = (
            'id',  'description_confectionary', 'filter_tag',
            'filter_mobile_code', 'mailing_start_time', 'mailing_end_time'
        )


class ReportMailingSerializer(serializers.ModelSerializer):
    """ Сериализатор модели сообщение """
    class Meta:
        model = Message
        fields = ('id',  'sending_time', 'status', 'id_mailing', 'id_client')
        depth = 1
