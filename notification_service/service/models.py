from django.core.validators import RegexValidator
from django.db import models


class Mailing(models.Model):
    mailing_start_time = models.DateTimeField(verbose_name="Дата и время запуска рассылки")
    description_confectionary = models.TextField(verbose_name="Текст сообщения для доставки клиенту")
    filter_tag = models.CharField(max_length=50, verbose_name="Фильтр свойств клиента (тег)", default=None)
    filter_mobile_code = models.CharField(
        max_length=50, verbose_name="Фильтр свойств клиента (код мобильного телефона,)", blank=True, default=None
    )
    mailing_end_time = models.DateTimeField(verbose_name="Дата и время окончания рассылки")

    class Meta:
        ordering = ["mailing_start_time"]
        verbose_name_plural = 'Рассылка'

    def __str__(self):
        return str(self.description_confectionary)


class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r'^[7]\d{10,12}$',
        message="Номер телефона должен быть введен в формате: '7ХХХХХХХХХХ'(Х - цифра от 0 до 9). Допускается 11 цифр."
    )
    phone = models.CharField(validators=[phone_regex], max_length=11, verbose_name="Номер телефона клиента")
    mobile_code = models.CharField(max_length=15, verbose_name="Код мобильного оператора")
    tag = models.CharField(max_length=20, verbose_name="Тег(произвольная метка)")
    time_zone = models.CharField(max_length=100, verbose_name="Часовой пояс")


class Message(models.Model):
    STATUS_MESSAGE = [
        ("generated", "создан"),
        ("sent", "отправлен"),
        ("sending_error", "ошибка"),
    ]

    sending_time = models.DateTimeField(verbose_name="Дата и время создания (отправки)")
    status = models.CharField(max_length=13, choices=STATUS_MESSAGE, verbose_name="Статус отправки", default='generated')
    id_mailing = models.ForeignKey(
        Mailing, on_delete=models.CASCADE, related_name='id_mailing', verbose_name='id рассылки'
    )
    id_client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name='id_client', verbose_name='id клиента, кому отправили сообщение'
    )
