from django_celery_beat.models import PeriodicTask, IntervalSchedule
from .models import Mailing, Client, Message
import pytz
import datetime
import requests
import json


def controll(dict_response):
    """Функция контроля переданного словаря"""
    if dict_response['code'] == 0:
        return True
    else:
        return False


def urls_request(text, number_phone, id_mailing):
    """Общая функция направления запросов по API"""

    id_mailing = int(id_mailing)
    phone = int(number_phone)

    url = f"https://probe.fbrq.cloud/v1/send/{str(id_mailing)}"

    payload = json.dumps({
        "id": id_mailing,
        "phone": phone,
        "text": text
    })

    headers = {
        'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTUyMTc4NDIsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Ik1heGltU08ifQ.tDJDcy9g3Cv8IQWkIglyMdgN7k2zY84GFLHcV8LzCnI',
        'Content-Type': 'application/json'
    }
    try:
        response = requests.request(method='POST', url=url, headers=headers, data=payload, timeout=25)
        dict_response = controll(dict_response=json.loads(response.text))
    except Exception:
        dict_response = False
    return dict_response


def mailing_order(text, number_phone, id_mailing):
    """Функция заносит неудачные запросы на повторное исполнение"""
    mailing = Mailing.objects.get(id=id_mailing)
    dict_response = urls_request(text, number_phone, id_mailing)
    if not dict_response:
        PeriodicTask.objects.create(
            name=f'mailing order {mailing.id}',
            task='repeat_order_message',
            interval=IntervalSchedule.objects.create(every=40, period='seconds'),
        )
    else:
        try:
            task = PeriodicTask.objects.get(name=f'mailing order {id_mailing}')
            task.enabled = False
            task.save()
        except Exception:
            pass
    return dict_response


def mailing_order_time(text, number_phone, id_mailing, start_time):
    """Функция заносит неудачные запросы на исполнение в определенное время"""
    dict_response = urls_request(text=text, number_phone=number_phone, id_mailing=id_mailing)
    if not dict_response:
        PeriodicTask.objects.create(
            name=f'mailing order {id_mailing}',
            task='repeat_order_message',
            interval=IntervalSchedule.objects.create(every=40, period='seconds'),
            start_time=start_time,
        )
    else:
        try:
            task = PeriodicTask.objects.get(name=f'mailing order {id_mailing}')
            task.enabled = False
            task.save()
        except Exception:
            pass
    return dict_response


def client_filter(tag=None, mobile_code=None):
    """Функция выбора клиента для рассылки"""
    list_client = Client.objects.filter(tag=tag, mobile_code=mobile_code)
    return list_client


def sending_messages(user_messages):
    """Функция рассылки сообщений"""
    instanse = Mailing.objects.get(id=user_messages['id'])
    list_client = client_filter(tag=instanse.filter_tag, mobile_code=instanse.filter_mobile_code)

    for i_client in list_client:
        instance_message = Message.objects.create(
            sending_time=datetime.datetime.now(),
            id_mailing=instanse,
            id_client=i_client
        )
        tz_client = pytz.timezone(i_client.time_zone)
        dt_client = datetime.datetime.now(tz_client)

        if instanse.mailing_start_time <= dt_client >= instanse.mailing_end_time:
            if mailing_order(id_mailing=instance_message.id,
                             number_phone=i_client.phone,
                             text=instanse.description_confectionary
                             ):
                instance_message.status = 'sent'
                instance_message.save()
            else:
                instance_message.status = 'sending_error'
                instance_message.save()

        elif instanse.mailing_start_time > dt_client:
            if mailing_order_time(id_mailing=instance_message.id,
                                  number_phone=i_client.phone,
                                  text=instanse.description_confectionary,
                                  start_time=instanse.mailing_start_time
                                  ):
                instance_message.status = 'sent'
                instance_message.save()
            else:
                instance_message.status = 'sending_error'
                instance_message.save()

        elif instanse.mailing_start_time > dt_client:
            instance_message.status = 'sending_error'
            instance_message.mailing_end_time = datetime.datetime.now()
            instance_message.save()
