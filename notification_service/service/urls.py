from django.urls import path
from . import views

app_name = 'service'

urlpatterns = [
    #client
    path('create_client/', views.CreateClient.as_view(), name='create_client'),
    path('update_client/<int:pk>/', views.UpdateClient.as_view(), name='update_client'),
    path('delete_client/<int:pk>/', views.DeleteClient.as_view(), name='delete_client'),
    #mailing
    path('create_mailing/', views.CreateMailing.as_view(), name='create_mailing'),
    path('mailing/', views.MailingViews.as_view(), name='mailing'),
    #report mailing
    path('report_mailing/', views.ReportMailing.as_view(), name='report_mailing'),
    path('detailed_newsletter_report/', views.DetailedNewsletterReport.as_view(), name='detailed_newsletter_report'),
    path('summary_newsletter_report/', views.SummaryNewsletterReport.as_view(), name='summary_newsletter_report'),
]
