from rest_framework.generics import CreateAPIView, UpdateAPIView, DestroyAPIView, GenericAPIView, ListAPIView
from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.http import JsonResponse
from .serializers import ClientSetSerializer, MailingSerializer, ReportMailingSerializer
from .models import Client, Mailing, Message
from . import utils
from rest_framework.response import Response


class CreateClient(CreateAPIView):
    """Класс добавления нового клиента"""
    permission_classes = (IsAuthenticated,)
    serializer_class = ClientSetSerializer


class UpdateClient(UpdateAPIView):
    """Класс обновления данных клиента"""
    queryset = Client.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ClientSetSerializer


class DeleteClient(DestroyAPIView):
    """Класс удаления клиента из справочника"""
    queryset = Client.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ClientSetSerializer


#Mailing
class CreateMailing(CreateAPIView):
    """Класс добавления новой рассылки"""
    permission_classes = (IsAuthenticated,)
    serializer_class = MailingSerializer

    def post(self, request, *args, **kwargs):
        user_mailing = super().post(request, *args, **kwargs)
        utils.sending_messages(user_messages=user_mailing.data)
        return user_mailing


class MailingViews(mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin,
                   GenericAPIView):
    """Класс:
    - просмотра информации о рассылки
    - обновления атрибутов рассылки,
    - удаления рассылки"""

    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        """просмотр информации о рассылки"""
        return self.retrieve(self, request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """обновление всех атрибутов рассылки"""
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        """обновление отдельных атрибутов рассылки"""
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """удаление рассылки"""
        return self.destroy(request, *args, **kwargs)


class ReportMailing(ListAPIView):
    """Отчет - все сообщения"""
    permission_classes = (IsAuthenticated,)
    queryset = Message.objects.all()
    serializer_class = ReportMailingSerializer


class DetailedNewsletterReport(APIView):
    """Подробный отчет о конктерной рассылке. Необходимо передать id_mailing (id рассылки)"""
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        queryset = Message.objects.filter(id_mailing=request.data['id_mailing'])
        serializer = ReportMailingSerializer(queryset, many=True)
        return Response(serializer.data)


class SummaryNewsletterReport(APIView):
    """Подробный отчет о конктерной рассылке. Необходимо передать id_mailing (id рассылки)"""
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        queryset = Message.objects.filter(id_mailing=request.data['id_mailing'])
        set_message = Message.objects.filter(id_mailing=request.data['id_mailing'], status='sent')
        error_message = Message.objects.filter(id_mailing=request.data['id_mailing'], status='sending_error')

        return JsonResponse({
            'Всего писем в рассылке': len(queryset),
            'Отправлено': len(set_message),
            'С ошибкой': len(error_message)
        })
