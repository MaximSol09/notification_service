from django.contrib.auth.models import User
from .serializers import CreateUserSerializer
from rest_framework_simplejwt.tokens import RefreshToken


def _get_tokens_for_user(user):
    """ Функция выдачи JWT токена """
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


def create_user(user_data):
    """Функция сохранения пользователя"""
    form = CreateUserSerializer(data=user_data)
    form.is_valid(raise_exception=True)
    create_user = User.objects.create_user(
        username=form.validated_data['username'],
        password=form.validated_data['password'],
    )
    jwt_token = _get_tokens_for_user(user=create_user)
    return jwt_token
