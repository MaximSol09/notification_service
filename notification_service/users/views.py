from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from django.http import JsonResponse
from . import utils
from drf_yasg.utils import swagger_auto_schema
from .serializers import CreateUserSerializer


class CreateUser(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=CreateUserSerializer)
    def post(self, request, *args, **kwargs):
        jwt_token = utils.create_user(request.data)
        return JsonResponse(jwt_token)
