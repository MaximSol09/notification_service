from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView,)
from .drf_yasg import urlpatterns as doc_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    #jwt token
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    #service
    path('service/', include('service.urls'), name='service'),
    #user
    path('', include('users.urls'),  name='users'),
]

urlpatterns += doc_urls
