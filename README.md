<h1> Сервис управления рассылками </h1>

<h2>Развернутывание</h2>

<p>Предпологается что у Вас уже установлена среда разработки(Pytharm или аналоги)</p>

<ol>
    <li>Скачать приложение</li>
    <li>Установить pipenv - pip install pipenv</li>
    <li>Войти в деррикторию с pipfile и выполнить команду - pipenv install</li>
    <li>По окончанию установки перейти в деррикторию с файлом manage.py</li>
    <li>Выполнить команду: python manage.py makemigrations</li>
    <li>Выполнить команду: python manage.py migrate</li>
    <li>Для создания суперпользователя выполнить команду: python manage.py createsuperuser</li>
    <li>Выполнить команду: python manage.py runserver</li>
</ol>

<p>Доступ к документации API осуществялется по адресу /docs/, альтернативно - /redoc/</p>
<p>Для начала работы необходимо зарегистрироваться и получить JWT токены </p>